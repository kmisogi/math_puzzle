#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Q04 棒の切り分け
長さn[cm]の棒を1[cm]単位に切り分けることを考えます。
1本の棒を1回で切ることができるのは1人だけです。

最大m人の人がいるとき、最短で何回で切り分けられるか、求めてください。
"""

import math

def run(max_length, num):
    result = [max_length]
    try_num = 0
    while max(result) > 1:
        result = split_bar(result, num)
        try_num += 1

    print('{0}[cm]の棒を{1}人で切り分けるときの最短施行回数は{2}です。\n'.format(max_length, num, try_num))


def split_bar(ary, num):
    """ 棒の分割 """
    result = []

    MIN_SPLIT_VAL = 2
    for idx, val in enumerate(ary):

        # MIN_SPLIT_VAL[cm]未満の場合は分割できない。
        if val < MIN_SPLIT_VAL:
            result.append(val)

        # 分割できる人が余っている場合
        elif idx < num:  # idxは0始まり
            result.append(math.floor(val/2))
            result.append(math.ceil(val/2))
        else:
            result.append(val)

    # 昇順にsort
    result.sort(reverse=True)
    return result


if __name__== '__main__':
    run(20, 3)
    run(100, 5)
