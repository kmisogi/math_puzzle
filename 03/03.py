#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""カードを裏返せ
1〜100までの番号が書かれた100枚のカードが順番に並べられています。
n番目のカードからn-1枚おきにカードを裏返す操作を、
どのカードの向きが変わらなくなるまで続けたとします。

カードの向きが変わらなくなったとき、
裏向きになっているカードの番号をすべて求めてください。
"""

def run():
    MIN = 2
    MAX = 100
    cards = [False] * MAX
    for n in range(MIN, MAX + 1):
        i = 1
        while i * n <= MAX:
            cards[i * n - 1] = not(cards[i * n - 1])
            i += 1
    result = []
    for i in range(0, MAX):
        if cards[i] == False:
            result.append(i + 1)
    print(result)


if __name__=="__main__":
    run()
