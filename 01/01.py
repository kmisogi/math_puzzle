#!/usr/bin/env python
# -*- coding: utf-8 -*-

u""" Q01 10進数で回文

[問題] 2進数、8進数、10進数いずれで表現しても回文となる数のうち、
10進数で10以上の数から最小の数を求めよ。
"""

import math

# N進数を取得する関数を生成するclass
class NNotation:
    def __init__(self, N):
        self.N = N
    def getNotation(self, num, notation):
        if num < self.N:
            notation = str(num) + notation
            return notation 
        else:
            notation = str(math.floor(num % self.N)) + notation
            return self.getNotation(math.floor(num / self.N), notation)

# 文字列を逆さにして返却する関数
def getReverse(num):
    return num[::-1]

if __name__=="__main__":
    num = 11
    while True:
        binary  = NNotation(2)
        octal   = NNotation(8)
        decimal = NNotation(10)
        resultBinary  = binary.getNotation(num, "")
        resultOcta1   = octal.getNotation(num, "")
        resultDecimal = decimal.getNotation(num, "")
        if (resultBinary == getReverse(resultBinary) 
                and resultOcta1 == getReverse(resultOcta1) 
                and resultDecimal == getReverse(resultDecimal)):
            print("### result ###")
            print("binary:" + resultBinary)
            print("octal:" + resultOcta1)
            print("decimal:" + resultDecimal)
            break
        num += 2
